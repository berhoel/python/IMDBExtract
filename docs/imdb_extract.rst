..
  Task: Documentation of imdb_extract

  :Authors:
    - `Berthold Höllmann <berhoel@gmail.com>`__
  :Organization: Berthold Höllmann
  :datestamp: %Y-%m-%d
  :Copyright: Copyright © 2022 by Berthold Höllmann

imdb_extract command
====================

.. sphinx_argparse_cli::
   :module: berhoel.imdb_extract
   :func: get_parser
   :prog: imdb_extract

..
  Local Variables:
  mode: rst
  compile-command: "make html"
  coding: utf-8
  End:
