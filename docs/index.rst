.. include:: ../README.rst

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   berhoel

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   imdb_extract
   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
