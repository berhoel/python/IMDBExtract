"""Write UTF-8 CSV files."""

from __future__ import annotations

import csv

UnicodeWriter = csv.writer
